/*
  ESP8266 Sketch by Eliseo Cortés
  For this exercise we will sense a current sensor with a relation of 100A/1V
  Then we need to measure this in an Arduino node-board.

  They are need some things to do that:
  1) Set the analog reference.
    Intructions says that de max operation voltage is 1V, so that is not a problem
  2) there is an error, the 10 bits-ADC works from 0-1023
*/
// Set the constants and variables

const int ADCpin = A0; // Esp8266 Analog pin
int inputValue = 0; // Set initial read value from sensor
float realCurrent = 0; // We need a float variable for decimals in current

void setup() {
  Serial.begin(115200);     // Initialize the Serial comunication to print in serial monitor
}


void loop() {
  //Read the ADC pin
  inputValue = analogRead(ADCpin);
  //Use a map funcion or the the rule realCurrent = inputValue*100/1023;
  realCurrent = map(inputValue, 0, 1023, 0, 100);

  // Print the information need.
  Serial.print("Corriente sensada: ");
  Serial.print(realCurrent,2); // It will be print 2 decimals of the current value
  Serial.println("/t A");
  
  delay(5000);                      // Wait for five seconds to get another sample
}
