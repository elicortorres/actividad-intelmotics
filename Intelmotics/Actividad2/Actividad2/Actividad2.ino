/*
    Code By Eliseo Cortés Torres on Dec 12th, 2020
    Objective: Create a test API which can request a POST message and
    then the server answer with 3 kind of messages.
      code: 200, msg: “Bienvenido a Intelmotics”
      code: 500, msg: “No se han enviado los campos correctamente”
      code: 400, msg: “El id y/o el token son incorrectos”

    The code performed is for the ESP8266 board.
 */


#include <ESP8266WiFiMulti.h>
#include <ESP8266HTTPClient.h>

const char* ssid = "EliseoHospot";
const char* password =  "12345678";

String id = "1212"; // Create Id as in documentation
String token = "1010101010"; // Create Token as in documentation

HTTPClient http;

// Due to we have 3 verifications I performed this function to call 3 times

void request(String datos_a_enviar) {
  //Send a POST message with the data required, function return int)
  int codigo_respuesta = http.POST(datos_a_enviar);   

    if(codigo_respuesta>0){
      Serial.println("Código HTTP ► " + String(codigo_respuesta));   //Print return code

      if(codigo_respuesta == 200){
        String cuerpo_respuesta = http.getString();
        Serial.println("El servidor respondió ▼ ");
        Serial.println(cuerpo_respuesta); // Answer of server

      }

    }else{

     Serial.print("Error enviando POST, código: ");
     Serial.println(codigo_respuesta); // Calling for error of message

    }
}

void setup() {
  delay(10);
  Serial.begin(115200); // Set the BaudRate


  WiFi.begin(ssid, password);

  Serial.print("Conectando...");
  while (WiFi.status() != WL_CONNECTED) { //Check for the connection
    delay(500);
    Serial.print(".");
  }

  Serial.print("Conectado con éxito, mi IP es: ");
  Serial.println(WiFi.localIP());

}

void loop() {

  if(WiFi.status()== WL_CONNECTED){   //Check WiFi connection status

    //Create a String to request msg 200
    String datos_m_200 = "id=" + id + "&token=" + token; 

    // Put the API Link
    http.begin("https://api.helo.v1.intelmotics.com/services/prueba_tecnica/hola.php");
    
    // Create yhe Header as specified in the document Headers: Content-Type: application/x-www-form-urlencoded
    http.addHeader("Content-Type", "application/x-www-form-urlencoded");

    // Request the 1st command
    request(datos_m_200);
    
    delay(5000); // Wait for 5 sec to request the other data
    // Create a String with Invalid Token and send it.
    String datos_m_500 = "id=" + id + "&token=" + "10101010";
    request(datos_m_500);
    
    delay(5000); // Wait for 5 sec to request the other data

    // Create a String with no data or "None" message and send it.
    String datos_m_400 = "none";
    request(datos_m_400);

    http.end();

  }else{

     Serial.println("Error en la conexión WIFI");

  }

   delay(5000);
}
